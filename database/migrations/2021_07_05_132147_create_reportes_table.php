<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Doctor_Name');
            $table->string('Doctor_Specialization');
            $table->string('Patint_Name');
            $table->integer('Patint_Id');
            $table->date('Birth_Date');
            $table->string('Disease_Name');
            $table->string('Description');
            $table->string('Tretment');
            $table->date('Date');
            $table->integer('Price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reportes');
    }
}
