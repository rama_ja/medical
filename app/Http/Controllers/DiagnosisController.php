<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Treatment;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class DiagnosisController extends Controller
{
    //$table->string('Disease');
    //            $table->string('Symptom');
    public function fill(){
        $b=new Diagnosis();
        $b->Disease="Diabetes";
        $b->Symptom="Urinate (pee) a lot, often at night";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Diabetes";
        $b->Symptom="Are very thirsty";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Diabetes";
        $b->Symptom="Lose weight without trying";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Diabetes";
        $b->Symptom="Have blurry vision";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Diabetes";
        $b->Symptom="Have very dry skin";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Underactive Thyroid";
        $b->Symptom="Have very dry skin";
        $b->save();


        /////////////////////

        $b=new Diagnosis();
        $b->Disease="Underactive Thyroid";
        $b->Symptom="Feel very tired";
        $b->save();


        $b=new Diagnosis();
        $b->Disease="Underactive Thyroid";
        $b->Symptom="being sensitive to cold";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Underactive Thyroid";
        $b->Symptom="muscle aches and weakness";
        $b->save();


        $b=new Diagnosis();
        $b->Disease="Underactive Thyroid";
        $b->Symptom="weight gain";
        $b->save();
////////////////////////////////////
///
      $b=new Diagnosis();
        $b->Disease="Brucellosis";
        $b->Symptom="Fever";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Brucellosis";
        $b->Symptom="Arthritis";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Brucellosis";
        $b->Symptom="Feel very tired";
        $b->save();

        $b=new Diagnosis();
        $b->Disease="Brucellosis";
        $b->Symptom="Lose weight without trying";
        $b->save();
/////////////////////////////////////////////////////////////////////////////
        $b=new Treatment();
        $b->Disease="Underactive Thyroid";
        $b->Treatment="An underactive thyroid (hypothyroidism) is usually treated by taking daily hormone replacement tablets called levothyroxine. Levothyroxine replaces the thyroxine hormone, which your thyroid does not make enough of. You'll initially have regular blood tests until the correct dose of levothyroxine is reached.";
        $b->save();

        $b=new Treatment();
        $b->Disease="Diabetes";
        $b->Treatment="Metformin is generally the preferred initial medication for treating type 2 diabetes, unless there's a specific reason not to use it. Metformin is effective, safe, and inexpensive. It may reduce the risk of cardiovascular events. Metformin also has beneficial effects when it comes to reducing A1C results.";
        $b->save();

        $b=new Treatment();
        $b->Disease="Brucellosis";
        $b->Treatment="Two-drug regimen consisting of streptomycin and doxycycline (streptomycin for 2 to 3 weeks and doxycycline for 8 weeks) or gentamicin plus doxycycline (gentamicin for 5-7 days and doxycycline for 8 weeks) should be recommended as the treatment of choice for uncomplicated brucellosis.";
        $b->save();

        $b=new Treatment();
        $b->Disease="Hepatitis C";
        $b->Treatment="Hepatitis C is treated using direct acting antiviral (DAA) tablets. DAA tablets are the safest and most effective medicines for treating hepatitis C. They're highly effective at clearing the infection in more than 90% of people. The tablets are taken for 8 to 12 weeks.";
        $b->save();

        $b=new Treatment();
        $b->Disease="AIDS";
        $b->Treatment="The main treatment for HIV is antiretroviral therapy, a combination of daily medications that stop the virus from reproducing. This helps protect CD4 cells, keeping the immune system strong enough to take measures against disease. Antiretroviral therapy helps keep HIV from progressing to AIDS.";
        $b->save();




        return "filled";
    }

public function show_symptoms()
{
    $all_de=Diagnosis::all();
    $tmp=[];
    foreach ($all_de as $de){
        if(!in_array($de->Symptom,$tmp)){
            array_push($tmp,$de->Symptom);
        }

    }

    return view('TheoreticalDiagnosis',['symptoms'=>$tmp]);
}

    public function get_desease(){

      $deseases=[];
      $all_de=Diagnosis::all();
      foreach ($all_de as $de){
          if(!in_array($de->Disease,$deseases)){
              array_push($deseases,$de->Disease);
          }

      }
        $result = array_unique($deseases);
//        foreach (request('symptoms') as $d) {
//            echo $d.'  /n  ';
//        }
        foreach ($deseases as $curr_de) {
            $counta=0;
            $countb=0;
           // $count=0;
            foreach ($all_de as $symp){
              //  $count++;
                if($symp->Disease==$curr_de){
                    if(in_array($symp->Symptom, request('symptoms'))){
                        $counta++;
                    }
                    $countb++;


                }
            }

            if($counta==$countb &&$counta==Diagnosis::all()->where('Disease',$curr_de)->count()&&sizeof(request('symptoms'))<=$countb){
              //  return $counta.$countb;
               // return Diagnosis::all()->where('Disease',$curr_de)->count() .$count;
                return view('DiagnosticResult',['desease'=>'The person has : '.$curr_de ,'treatment'=>Treatment::where('Disease',$curr_de)->firstOrFail()->Treatment]);
            }
        }
       // return "ggggggggggggggggggg";
        return view('DiagnosticResult',['desease'=>' There is no specific disease','treatment'=>'no']);

    }

    public function visual_d()
    {
//       // $fields = $request->radio;
//        $fields = $request->radio1;
//        $fields .= $request->radio2;
//        $fields .= $request->radio3;
//        $fields .= $request->radio4;

        if(request('eye')==2
        &&request('skin')==2
        &&request('leg')==2
        &&request('tongue')==1
        &&request('hands')==1
        &&request('temperature')!=1
        &&request('face')==1
        &&request('Itch')==1
        &&request('Arthritis')!=1
        ){if(Treatment::where('Disease','Hepatitis C')->count()!=0){
            return view('DiagnosticResult',['desease'=>'The person has : Hepatitis C ','treatment'=>Treatment::where('Disease','Hepatitis C')->firstOrFail()->Treatment]);
        }else{
            return view('DiagnosticResult',['desease'=>'The person has : Hepatitis C ','treatment'=>'no']);

        }

        }
        elseif (request('eye')==1
            &&request('skin')==1
            &&request('leg')==1
            &&request('tongue')==2
            &&request('hands')==2
            &&request('temperature')==1
            &&request('face')==2
            &&request('Itch')!=1
            &&request('Arthritis')!=1
        ){if(Treatment::where('Disease','AIDS')->count()!=0){
            return view('DiagnosticResult',['desease'=>'The person has : AIDS ','treatment'=>Treatment::where('Disease','AIDS')->firstOrFail()->Treatment]);
        }else{
            return view('DiagnosticResult',['desease'=>'The person has : AIDS ','treatment'=>'no']);

        }

        }
        elseif (request('eye')==1
            &&request('skin')==1
            &&request('leg')==1
            &&request('tongue')==1
            &&request('hands')==1
            &&request('temperature')==1
            &&request('face')==1
            &&request('Itch')!=1
            &&request('Arthritis')==1
        ){if(Treatment::where('Disease','Brucellosis')->count()!=0){
            return view('DiagnosticResult',['desease'=>'The person has : Brucellosis ','treatment'=>Treatment::where('Disease','Brucellosis')->firstOrFail()->Treatment]);
        }else{
            return view('DiagnosticResult',['desease'=>'The person has : Brucellosis ','treatment'=>'no']);

        }
        }
        else{
            return view('DiagnosticResult',['desease'=>' There is no specific disease','treatment'=>'no']);
        }

      //  return $fields;
    }
}
