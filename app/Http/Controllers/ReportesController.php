<?php

namespace App\Http\Controllers;

use App\Reportes;
use App\Contacts;

use App\Schedules;
use Illuminate\Http\Request;

class ReportesController extends Controller
{
    public function show()
{  if(Reportes::all()->count()!=0){
    return view('Reportes', ['re' => Reportes::all()]);
  } else{
    return view('NoReporte',['x'=>"There is no reportes"]);
}

}
    public function fill(){
        $b=new Reportes();
        $b->Doctor_Name="Dr Ahmad";
        $b->patint_Name="naia";
        $b->Disease_Name="Diabetes";
        $b->Description="Sudden rise in blood sugar";
        $b->Tretment="Insulin";
        $b->Date=date('2020-5-5');
        $b->Price=int('7000');
        $b->save();
return "filled";
    }
    public function create()
    {
        $rep=new Reportes();
        $rep->Doctor_Name=request('Doctor');
        $rep->Doctor_Specialization=request('Doctor_Specialization');
        $rep->Patint_Name=request('Patint');
        $rep->Patint_Id=request('Patint_Id');
        $rep->Birth_Date=date(request('Birth_Date'));
        $rep->Disease_Name=request('Disease');
        $rep->Description=request('Description');
        $rep->Tretment=request('Tretment');
        $rep->Date=date(request('date'));
        $rep->Price=request('Price');
        $rep->save();

        return redirect('/show');

    }
    public function create_contact()
    {
        $rep=new Contacts();
        $rep->Name=request('Name');
        $rep->Email=request('Email');
        $rep->Phone=request('Phone');
        $rep->Subject=request('Subject');
        $rep->Message=request('Message');
        $rep->save();

        return redirect()->back();

    }
    public function show_contact(){
        if(Contacts::all()->count()!=0){
            return view('Contact', ['re' => Contacts::all()]);
        } else{
            return view('NoReporte',['x'=>"There is no messages"]);
        }
    }
    public function search()
    {if(Reportes::all()->where('Patint_Id',request('Patint_Id'))->count()!=0) {
        $br=Reportes::all()->where('Patint_Id',request('Patint_Id'));
        return view('PatintReporteDisplay',['re'=>$br]);
    }
    else{
        return view('NoReporte',['x'=>"There is no reportes for entered patint"]);
    }
    }
    public function create_Appointment()
    {
        $rep=new Schedules();
        $rep->Doctor_Name_s=request('Doctor_Name_s');
        $rep->Patint_Name_s=request('Patint_Name_s');
        $rep->Date_s=date(request('Date_s'));
        $rep->save();

        return redirect()->back();

    }
    public function show_Appointment(){
//        if(Schedules::all()->count()!=0){
//
//        } else{
//            return view('NoReporte',['x'=>"There is no Appointments"]);
//        }
        $events = Schedules::orderBy('Date_s', 'asc')->get();
        return view('Schedule', ['re' =>$events]);
    }

    public function delete_reporte()
    { if(Reportes::find(request('id'))!=null)
    {$br=Reportes::find(request('id'))->delete();

        return redirect()->back();
    } else{
        return view('NoReporte',['x'=>"This reporte not exist"]);
    }

    }
    public function delete_message()
    {  if(Contacts::find(request('id'))!=null)
    {$br=Contacts::find(request('id'))->delete();

        return redirect()->back();
    } else{
        return view('NoReporte',['x'=>"This message not exist"]);
    }

    }
    public function delete_appointment()
    {
        if(Schedules::find(request('id'))!=null)
        { $br=Schedules::find(request('id'))->delete();

            return redirect()->back();
        } else{
            return view('NoReporte',['x'=>"This appointment not exist"]);
        }

    }
}
