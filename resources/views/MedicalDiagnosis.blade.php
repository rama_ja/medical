<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Life Care </title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="images/fevicon.ico.png" type="image/x-icon" />
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="css/custom.css">
<link rel="stylesheet" href="css/all.min.css" >
<link rel="stylesheet" href="css/all.css" >
<!-- Modernizer for Portfolio -->
<script src="js/modernizer.js"></script>

<!-- [if lt IE 9] -->
</head>
<body class="clinic_version">
<!-- LOADER -->
<div id="preloader">
    <img class="preloader" src="images/loaders/heart-loading2.gif" alt="">
</div>
<!-- END LOADER -->
<header>
    <div class="header-top wow fadeIn">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="image"></a>
            <div class="right-header">
                <div class="header-info">

                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom wow fadeIn">
        <div class="container">
            <nav class="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="navbar"><i class="fa fa-bars" aria-hidden="true"></i></button>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/"><img src="images/home.jpg"></a></li>


                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

<!-- end section -->

<div id="home" class="parallax first-section wow fadeIn" data-stellar-background-ratio="0.4" >
    <div class="text-contant">
        <div class="center">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">
                        <a href="/theoretical_diagnosis"><h3>Theoretical Diagnosis</h3><img src="images/Theoretical.jpg" width="400" height="400"></a>
                    </div>
                    <div class="col-md-4 col-lg-offset-2">
                        <a href="/visual_diagnosis" ><h3>Visual Diagnosis</h3><img src="images/Visual.jpg"  width="400" height="400"></a>
                    </div>




                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>






<!-- end copyrights -->
<a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
<!-- all js files -->
<script src="js/all.js"></script>
<!-- all plugins -->
<script src="js/custom.js"></script>
<!-- map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNUPWkb4Cjd7Wxo-T4uoUldFjoiUA1fJc&callback=myMap"></script>
</body>
</html>
