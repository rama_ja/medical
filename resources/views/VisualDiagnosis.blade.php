<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Life Care </title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="images/fevicon.ico.png" type="image/x-icon" />
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="css/custom.css">
<link rel="stylesheet" href="css/all.min.css" >
<link rel="stylesheet" href="css/all.css" >
<!-- Modernizer for Portfolio -->
<script src="js/modernizer.js"></script>

<!-- [if lt IE 9] -->
</head>
<body class="clinic_version">
<!-- LOADER -->
<div id="preloader">
    <img class="preloader" src="images/loaders/heart-loading2.gif" alt="">
</div>
<!-- END LOADER -->
<header>
    <div class="header-top wow fadeIn">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="image"></a>
            <div class="right-header">
                <div class="header-info">

                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom wow fadeIn">
        <div class="container">
            <nav class="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="navbar"><i class="fa fa-bars" aria-hidden="true"></i></button>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/"><img src="images/home.jpg"></a></li>


                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

<!-- end section -->

<div id="home" class="parallax first-section wow fadeIn" >
    <div class="text-contant">
        <img src="images/Visual.jpg" width="800" height="500">
        <!-- end row -->
    </div>
    <!-- end container -->
</div>

<div id="home" class="col-md-offset-2">


<form method="POST" action="/visual_d" >
    {!! csrf_field() !!}
    <h1>Select Symptoms</h1>
    <div class="container ">
        <div class="col-md-4"><img src="images/desease/healthy eye.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="radio"  name="eye" class="form-check-input" value="1" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="col-md-4" ><img src="images/desease/sick eye.jpg" width="300" height="300">
            <label class="form-check">select it
                <input type="radio" name="eye" class="form-check-input" value="2" required="required">
                <span class="checkmark"></span>
            </label>
        </div>

    </div><br>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/healthy skin.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="radio"  name="skin" class="form-check-input" value="1" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="col-md-4" >
            <img src="images/desease/sick skin.jpg" width="300" height="300">
            <label class="form-check">select it
                <input type="radio" name="skin" class="form-check-input" value="2" required="required">
                <span class="checkmark"></span>
            </label>
        </div>

    </div><br>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/healthy leg.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="radio"  name="leg" class="form-check-input" value="1" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="col-md-4" >
            <img src="images/desease/sick leg.jpg" width="300" height="300">
            <label class="form-check">select it
                <input type="radio" name="leg" class="form-check-input" value="2" required="required">
                <span class="checkmark"></span>
            </label>
        </div>

    </div><br>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/healthy tongue.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="radio"  name="tongue" class="form-check-input" value="1" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="col-md-4" >
            <img src="images/desease/sick tongue.jpg" width="300" height="300">
            <label class="form-check">select it
                <input type="radio" name="tongue" class="form-check-input" value="2" required="required">
                <span class="checkmark"></span>
            </label>
        </div>

    </div><br>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/healthy hands.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="radio"  name="hands" class="form-check-input" value="1" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="col-md-4" >
            <img src="images/desease/sick hands.jpg" width="300" height="300">
            <label class="form-check">select it
                <input type="radio" name="hands" class="form-check-input" value="2" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/body temperature rose.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="checkbox"  name="temperature" class="form-check-input" value="1">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/healthy face.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="radio"  name="face" class="form-check-input" value="1" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="col-md-4" >
            <img src="images/desease/sick face.jpg" width="300" height="300">
            <label class="form-check">select it
                <input type="radio" name="face" class="form-check-input" value="2" required="required">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/Itch.jpg" width="300" height="300" >
            <label  class="form-check">select it
                <input type="checkbox"  name="Itch" class="form-check-input" value="1">
                <span class="checkmark"></span>
            </label>
        </div>

    </div>
    <div class="container ">
        <div class="col-md-4">
            <img src="images/desease/Arthritis.jpg" width="300" height="300" >
        </div>
        <div class="col-md-4">
            <img src="images/desease/Arthritis1.jpg" width="300" height="300" >
        </div>
        <div class="col-md-4">
            <img src="images/desease/Arthritis2.jpg" width="300" height="300" >
        </div>
            <label  class="form-check">select it
                <input type="checkbox"  name="Arthritis" class="form-check-input" value="1">
                <span class="checkmark"></span>
            </label>
        </div>


    <input type="submit" value="Submit">
</form>


</div>
  <!--  <form action="/get_desease" method="POST">
        { !! csrf_field() !!}


        <label class="container">Three
            <input type="radio" name="radio">
            <span class="checkmark"></span>
        </label>
        <label class="container">Four
            <input type="radio" name="radio">
            <span class="checkmark"></span>
        </label>

        <br>

        <input type="submit" value="Submit">

    </form>-->




<footer id="footer" class="footer-area wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="logo padding">
                    <a href=""><img src="images/logo.png" alt=""></a>
                    <p>Locavore pork belly scen ester pine est chill wave microdosing pop uple itarian cliche artisan.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-info padding">
                    <h3>CONTACT US</h3>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> PO Box 16122 Collins Street West Victoria 8007 Australia</p>
                    <p><i class="fa fa-paper-plane" aria-hidden="true"></i> info@gmail.com</p>
                    <p><i class="fa fa-phone" aria-hidden="true"></i> (+1) 800 123 456</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="subcriber-info">
                    <h3>SUBSCRIBE</h3>
                    <p>Get healthy news, tip and solutions to your problems from our experts.</p>
                    <div class="subcriber-box">
                        <form id="mc-form" class="mc-form">
                            <div class="newsletter-form">
                                <input type="email" autocomplete="off" id="mc-email" placeholder="Email address" class="form-control" name="EMAIL">
                                <button class="mc-submit" type="submit"><i class="fa fa-paper-plane"></i></button>
                                <div class="clearfix"></div>
                                <!-- mailchimp-alerts Start -->
                                <div class="mailchimp-alerts">
                                    <div class="mailchimp-submitting"></div>
                                    <!-- mailchimp-submitting end -->
                                    <div class="mailchimp-success"></div>
                                    <!-- mailchimp-success end -->
                                    <div class="mailchimp-error"></div>
                                    <!-- mailchimp-error end -->
                                </div>
                                <!-- mailchimp-alerts end -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>



<!-- end copyrights -->
<a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
<!-- all js files -->
<script src="js/all.js"></script>
<!-- all plugins -->
<script src="js/custom.js"></script>
<!-- map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNUPWkb4Cjd7Wxo-T4uoUldFjoiUA1fJc&callback=myMap"></script>
</body>
</html>
