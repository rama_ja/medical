<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Diagnosis;

Route::get('/w', function () {
    return view('welcome');
});
//Route::get('/', 'LibController@pcon');

Route::get('/report_system', function () {
    return view('MedicalReportSystem');
});
Route::get('/', function () {
    return view('HomePage');
});
Route::get('/medical_diagnosis', function () {
    return view('MedicalDiagnosis');
});
Route::get('/visual_diagnosis', function () {
    return view('VisualDiagnosis');
});
Route::get('/theoretical_diagnosis','DiagnosisController@show_symptoms');
Route::post('/get_desease','DiagnosisController@get_desease');
Route::post('/visual_d','DiagnosisController@visual_d');

//Route::get('/show', function () {
//    return view('Reportes',['re'=>\App\Reportes::all()]);
//});
Route::get('/add_reporte', function () {
    return view('AddReporte');
});
Route::get('/patint_reporte', function () {
    return view('PatintReporte');
});
Route::post('/add','ReportesController@create');
Route::post('/add_contact','ReportesController@create_contact');
Route::post('/search','ReportesController@search');
Route::post('/add_appointment','ReportesController@create_Appointment');
Route::get('/show', 'ReportesController@show');
Route::get('/show_contact', 'ReportesController@show_contact');
Route::get('/show_appointment', 'ReportesController@show_Appointment');

Route::post('/delete_reporte','ReportesController@delete_reporte');
Route::post('/delete_message','ReportesController@delete_message');
Route::post('/delete_appointment','ReportesController@delete_appointment');

Route::get('/f', 'DiagnosisController@fill');